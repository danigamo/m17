﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSceneManager : MonoBehaviour
{
	#region Unity Fields
	public Camera mainCamera;
	public PlayerController player;
    public Camera mainCamera2;
    public PlayerController2 player2;
    public Text scoreText;
	public Text gameOverText;
    public Text lapText;
    public Text scoreText2;
    public Text gameOverText2;
    public Text lapText2;
    public Transform checkpointContainer;
	#endregion Unity Fields

	#region Fields
	private float gameTimer;
	private bool gameOver;
	public int nCheckpoints;
	public int currentCheckpoint;
	private float trackStartTime;
	public bool started;
    private int nLaps;
    private float totTime;
    private float gameTimer2;
    private bool gameOver2;
    public int currentCheckpoint2;
    private float trackStartTime2;
    private int nLaps2;
    private float totTime2;

    public MejorVuelta bt;
    public MejorVuelta bt2;
    #endregion Fields

    #region Methods
    public void Start ()
	{
		Time.timeScale = 1;

        //ens suscribim a tots els checkpoints
        nCheckpoints = checkpointContainer.childCount;
		foreach (CheckpointController checkpoint in checkpointContainer.GetComponentsInChildren<CheckpointController>())
		{
			checkpoint.OnHitPlayer += OnReachCheckpoint;
		}

        currentCheckpoint = -1;
        nLaps = 1;
        totTime = 0;
		scoreText.text = "";
        lapText.text = "";

        currentCheckpoint2 = -1;
        nLaps2 = 1;
        totTime2 = 0;
        scoreText2.text = "";
        lapText2.text = "";
    }

	public void Update ()
	{

        //si la "carrera" esta iniciada colocamos el texto de las vueltas, el tiempo y, si hay, el del mejor tiempo
		if (started)
		{
            
            lapText.text = "Laps: " + nLaps+ "/3";
            scoreText.text = "Temps: " + (Time.time - trackStartTime).ToString("F2");
			if (bt.bestTime > 0)
			{
				scoreText.text += "\nMillor: " + (bt.bestTime.ToString("F2"));
			}

            lapText2.text = "Laps: " + nLaps + "/3";
            scoreText2.text = "Temps: " + (Time.time - trackStartTime).ToString("F2");
            if (bt2.bestTime > 0)
            {
                scoreText2.text += "\nMillor: " + (bt2.bestTime.ToString("F2"));
            }
        }
	}

    //el texto cambiara y dira quien ha ganado
    public void OnGameWin(string player)
    {
        scoreText.enabled = false;
        lapText.enabled = false;
        scoreText.enabled = false;
        lapText.enabled = false;
        scoreText2.enabled = false;
        lapText2.enabled = false;
        gameOverText2.enabled = true;
        gameOverText.enabled = true;
        gameOverText2.enabled = true;
        if (player.Equals("Player"))
        {
            gameOverText.text = "YU AR E GUINER";
            gameOverText2.text = "LOSER";
        }
        else
        {
            gameOverText.text = "LOSER";
            gameOverText2.text = "YU AR E GUINER";
        }

        Time.timeScale = 0;
	}

    //en el momento en que llegues a un checkpoint cambiara la variable del checkpoint actual para ir al siguiente
    //o eso se supone a mi no me funciona y me quiero morir
	private void OnReachCheckpoint (CheckpointController checkpoint, string player)
    {
        if (player.Equals("Player"))
        {
            if (currentCheckpoint == -1 && checkpoint.id == 0)
            {
                started = true;
                trackStartTime = Time.time;
                currentCheckpoint = checkpoint.id;
            }
            else if (checkpoint.id == 0 && currentCheckpoint == nCheckpoints - 1)
            {
                OnFullTrack("Player");
                currentCheckpoint = checkpoint.id;
            }
            else if (checkpoint.id == currentCheckpoint + 1)
            {
                currentCheckpoint = checkpoint.id;
            }
        }
        else
        {
            if (currentCheckpoint2 == -1 && checkpoint.id == 0)
            {
                started = true;
                trackStartTime2 = Time.time;
                currentCheckpoint2 = checkpoint.id;
            }
            else if (checkpoint.id == 0 && currentCheckpoint2 == nCheckpoints - 1)
            {
                OnFullTrack("Player2");
                currentCheckpoint2 = checkpoint.id;
            }
            else if (checkpoint.id == currentCheckpoint2 + 1)
            {
                currentCheckpoint2 = checkpoint.id;
            }
        }
    }

    //al completar el "circuito" cambiaremos la variable de mejor vuelta y aumentaremos en uno el numero de vueltas hechas
    private void OnFullTrack(string player)
    {
        if (player.Equals("Player"))
        {
            float currentTime = Time.time - trackStartTime;
            if (bt.bestTime == 0 || currentTime < bt.bestTime)
            {
                bt.bestTime = currentTime;
            }
            totTime += currentTime;
            trackStartTime = Time.time;
            nLaps++;
            if (nLaps == 3)
            {
                this.OnGameWin(player);
            }
        }
        else
        {
            float currentTime = Time.time - trackStartTime2;
            if (bt2.bestTime == 0 || currentTime < bt2.bestTime)
            {
                bt2.bestTime = currentTime;
            }
            totTime2 += currentTime;
            trackStartTime2 = Time.time;
            nLaps2++;
            if (nLaps2 == 3)
            {
                this.OnGameWin(player);
            }
        }
    }
    #endregion Methods
}