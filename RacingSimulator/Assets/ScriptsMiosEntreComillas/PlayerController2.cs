﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour
{
    #region Constants
    private float SPEED = 100000f;//velocidad de nuestros barcos
    private float ROTATE_SPEED = 20f;//velocidad de rotacion de nuestros barcos
    public bool suelo = false;//boleano para detectar el suelo
    #endregion Constants


    #region Methods
    public void FixedUpdate()
    {
        ProcessInput();
    }

    //Imput del barco
    private void ProcessInput()
    {
        if (suelo == true)
        {
            if (Input.GetKey("left"))
            {
                transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
            }
            if (Input.GetKey("right"))
            {
                transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
            }

            if (Input.GetKey("up")) { this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime); }
            if (Input.GetKey("down")) { this.GetComponent<Rigidbody>().AddForce(-this.transform.forward * SPEED * Time.deltaTime); }
        }
    }

    //Mientras esta en el agua se podra mover, de lo contrario se quedara quieto
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Agua")
        {
            suelo = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.name.Equals("Agua"))
        {
            suelo = false;
        }
    }
    #endregion Methods
}
