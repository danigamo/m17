﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public Player player;
    public Camera gameCamera;
    public Text score;
    public static int puntos=(-100);

    //todos los prefabs disponibles
    public GameObject[] blockPrefabs;
    //puntero, apunta el final de los bloques generados
    private float puntero;
    //spawn, la variable extra que tiene que "mirar" el puntero para saber si generar un nuevo bloque
    private float spawn = 20;
    // Start is called before the first frame update
    void Start()
    {
        //justo al salir creamos un bloque inicial
        Instantiate(blockPrefabs[0]);
        score.text = "NaRuToOoOoOo";
        score.color = Color.black;
        score.fontSize = 15;
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {

        score.text="Score: "+puntos;
        score.color=Color.black;
        score.fontSize = 35;

        //hacemos que la camera siga al jugador. La camara usa un vector3
        gameCamera.transform.position = new Vector3(
            player.transform.position.x+5,
            player.transform.position.y+1,
            gameCamera.transform.position.z);

        if (player != null && this.puntero < player.transform.position.x + spawn )
        {
            puntos+=100;
            //instanciar hace aparecer en la escena activa
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            float size = newBlock.transform.GetChild(0).transform.localScale.x;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            newBlock.transform.position = new Vector2(puntero+size/2, 0);
            //aumentamos el puntero en el tamaño del bloque
            puntero += size;
        }
    }
}
