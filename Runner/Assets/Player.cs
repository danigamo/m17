﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{
    public int jumpForce;
    public int horizontalVelocity;
    private bool salta=false;
    public bool suelo;
    // Start is called before the first frame update


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Animator>().SetBool("Suelo", suelo);
        if (Input.GetKeyDown("space"))
        {
            salto();

        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);


        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            moriste();
        }
    }
    

    private void salto()
    {
        if (salta){
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            salta = false;
            suelo=false;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        
        if (coll.gameObject.tag == "Obstacle")
        {
            GameObject.Destroy(this.gameObject);
            moriste();
        }
        Debug.Log(gameObject.tag);
        if (coll.gameObject.tag == "Floor")
        {
            salta = true;
            suelo=true;
        }

    }

    private void moriste()
    {
        SceneManager.LoadScene("Fin");
    }

}
