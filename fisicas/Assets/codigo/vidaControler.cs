﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vidaControler : MonoBehaviour
{ 
    public boboboControler bobo;
    public donpatchcontroller donp;
    public GameObject VidaBo;
    public GameObject VidaDp;
    // Start is called before the first frame update
    void Start()
    {
        bobo.OnDamagedEvent += BoBoDamage;
        donp.OnDamagedEvent += DonpDamage;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //cambiamos el tamaño de la barra de vida segun el daño que se haga a cada personaje
    void BoBoDamage(float hp)
    {
        VidaBo.transform.localScale = new Vector2(hp / 100, VidaBo.transform.localScale.y);
    }
    void DonpDamage(float hp)
    {
        VidaDp.transform.localScale = new Vector2(hp / 100, VidaDp.transform.localScale.y);
    }
}

