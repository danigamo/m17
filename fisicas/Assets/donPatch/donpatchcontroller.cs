﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class donpatchcontroller : MonoBehaviour
{
    // Start is called before the first frame update
    public int vel;
    public int fsalto;
    private bool salta = false;
    public bool suelo;
    public bool block=false;
    public int status;
    private bool cBreak;
    private bool pego;
    public Animator anim;
    public GameObject misil;
    public float vida = 100;
    public delegate void OnDamaged(float vida);
    public event OnDamaged OnDamagedEvent;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (vida <= 0)
        {
            StartCoroutine(fin(3));
            this.GetComponent<Animator>().SetTrigger("F");
            
        }
        else
        {
            anim.SetFloat("vel", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));


            if (Input.GetKey("right"))
            {
                //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
                transform.localScale = new Vector3(1f, 1f, 1f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

            }
            else if (Input.GetKey("left"))
            {
                //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));
                transform.localScale = new Vector3(-1f, 1f, 1f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (Input.GetKeyDown("[2]"))
            {
                anim.SetTrigger("shoot");
                StartCoroutine(disparo());

            }
            else if (Input.GetKeyDown("[1]"))
            {
                if (status == 1)
                {
                    pego = true;
                    GameObject ataque = this.gameObject.transform.GetChild(2).gameObject;
                    ataque.SetActive(true);
                    anim.SetTrigger("golpe2");
                    StartCoroutine(golpea(1f));
                }
                else
                {
                    cBreak = true;
                    status = 0;
                }
                if (status == 0 && !pego)
                {
                    pego = true;
                    GameObject ataque = this.gameObject.transform.GetChild(2).gameObject;
                    ataque.SetActive(true);
                    anim.SetTrigger("golpe");
                    cBreak = false;
                    StartCoroutine(MaxCombo(0.4f, status));
                    StartCoroutine(MinCombo(0.1f, 1));
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                    StartCoroutine(golpea(0.8f));
                }
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                anim.SetBool("block", false);
            }

            if (Input.GetKey("up"))
            {
                if (salta)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fsalto));
                    salta = false;
                    anim.SetBool("jump", true);

                }
            }

            if (Input.GetKey("down"))
            {
                anim.SetBool("block", true);
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
                vel = 1;
                block = true;
            }
            else
            {
                anim.SetBool("block", false);
                vel = 10;
                block = false;
            }

        }

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Floor"))
        {
            salta = true;
            anim.SetBool("jump", false);

        }
        if (coll.gameObject.CompareTag("OUT"))
        {
            anim.SetTrigger("OUT");
            fuerisima();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "golpeB")
        {
            if (this.transform.localScale.x == 1.2f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(450f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-450f, 150f));
            }

            if (!block)
            {
                vida -= 5;
                if (vida <= 0)
                {
                    vida = 0;
                    anim.SetTrigger("F");

                }
                OnDamagedEvent(vida);

                anim.SetTrigger("AY");
            }
        }

        if (collision.gameObject.name == "misilb(Clone)")
        {
            if (this.transform.localScale.x == 1.2f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(4, 1));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-4, 1));
            }
            if (!block)
            {
                vida -= 10;
                if (vida <= 0)
                {
                    vida = 0;
                    anim.SetTrigger("F");
                    Destroy(this);
                }
                OnDamagedEvent(vida);
            }

        }

    }
    
    IEnumerator disparo()
    {
        yield return new WaitForSeconds(1);
        GameObject newMisil = Instantiate(misil);
        newMisil.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y+1);
    }

    IEnumerator golpea(float v)
    {
        yield return new WaitForSeconds(v);
        if (pego == true)
        {
            pego = false;
        }
    }

    IEnumerator MinCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status != st)
        {
            status = 0;
            cBreak = true;
        }
    }


    IEnumerator fin(float f)
    {
        yield return new WaitForSeconds(f);
        SceneManager.LoadScene("winBo");
    }

    public void fuerisima()
    {
        vida = 0;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
    }
}