﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class boboboControler : MonoBehaviour
{
    // Start is called before the first frame update
    public int vel;
    public int fsalto;
    private bool salta = false;
    public bool suelo;
    public bool block = false;
    public int status;
    private bool cBreak;
    private bool pego;
    public Animator anim;
    public GameObject misil;
    public float vida = 100;
    public delegate void OnDamaged(float vida);
    public event OnDamaged OnDamagedEvent;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si la vida es 0 el personaje fallece, sino te deja hacer cosas
        if (vida <= 0)
        {
            StartCoroutine(fin(3));
            this.GetComponent<Animator>().SetTrigger("F");
   
        }
        else
        {
            anim.SetFloat("vel", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));

            //si pulsas W saltas
            if (Input.GetKey("w"))
            {
                if (salta)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fsalto));
                    salta = false;
                    anim.SetBool("jump", true);

                }
            }
            //si se pulsa D te mueves a la izquierda
            else if (Input.GetKey("d"))
            {
                //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
                transform.localScale = new Vector3(1f, 1f, 1f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

            }
            //si se pulsa A te mueves a la derecha
            else if (Input.GetKey("a"))
            {
                //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));
                transform.localScale = new Vector3(-1f, 1f, 1f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            // si pulsas M haces un ataque a distancia
            else if (Input.GetKeyDown("m"))
            {
                anim.SetTrigger("shoot");
                StartCoroutine(disparo());

            }
            //si pulsas N haces un ataque a mele
            else if (Input.GetKeyDown("n"))
            {
                //si antes de que acabe el primer ataque pulsas N otra vez haces un segundo ataque
                if (status == 1)
                {
                    pego = true;
                    anim.SetTrigger("golpe2");
                    StartCoroutine(golpea(1.2f));
                }
                else
                {
                    cBreak = true;
                    status = 0;
                }//haces el primer ataque
                if (status == 0 && !pego)
                {
                    pego = true;
                    anim.SetTrigger("golpe");
                    cBreak = false;
                    StartCoroutine(MaxCombo(0.4f, status));
                    StartCoroutine(MinCombo(0.2f, 1));
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                    StartCoroutine(golpea(0.8f));
                }


            }
            //si no pulsas nada te quedas quieto
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                anim.SetBool("block", false);
            }


            //si pulsas S bloqueas todos los golpes que recibas
            if (Input.GetKey("s"))
            {
                anim.SetBool("block", true);
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
                vel = 1;
                block = true;
            }
            else
            {
                anim.SetBool("block", false);
                vel = 10;
                block = false;
            }
        }


    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        //al colisionar con el suelo te deja volver a saltar, sino no puedes. asi evitamos el doble salto
        if (coll.gameObject.CompareTag("Floor"))
        {
            salta = true;
            anim.SetBool("jump", false);

        }
        //si te sales del mapa del Smash mueres al instante
        if (coll.gameObject.CompareTag("OUT"))
        {
            anim.SetTrigger("OUT");
            fuerisima();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //al colisionar con la hitbox del golpe de DonPatch te mueve y recibes daño
        if (collision.gameObject.name == "golpeD")
        {
            if (this.transform.localScale.x == 1.2f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(150f, 50f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-150f, 50f));
            }
            //si no estas bloqueando recibes el daño
            if (!block)
            {
                vida -= 5;
                if (vida <= 0)
                {
                    vida = 0;
                    anim.SetTrigger("F");
                }
                OnDamagedEvent(vida);

                anim.SetTrigger("AY");
            }
        }
        //al colisionar contra el proyectil del enemigo recibes daño si no lo bloqueas
        if (collision.gameObject.name == "misild(Clone)")
        {
            if (!block)
            {
                vida -= 10;
                if (vida <= 0)
                {
                    vida = 0;
                    anim.SetTrigger("F");
                    Destroy(this);
                }
                OnDamagedEvent(vida);
            }

        }
    }

    //esperamos que la animacion de disparar el proyectil para iniciar el prefab de los proyectiles y crearlos delante nuestra
    IEnumerator disparo()
    {
        yield return new WaitForSeconds(1);
        GameObject newMisil = Instantiate(misil);
        newMisil.transform.position = new Vector2(this.transform.position.x + 1, this.transform.position.y + 1);
    }

   
    IEnumerator golpea(float v)
    {
        yield return new WaitForSeconds(v);
        if (pego == true)
        {
            pego = false;
        }
    }

    IEnumerator MinCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status != st)
        {
            status = 0;
            cBreak = true;
        }
    }

    //al morir pasamos a la escena del vistoria de DonPatch
    IEnumerator fin(float f)
    {
        yield return new WaitForSeconds(f);

        SceneManager.LoadScene("winDp");
    }

    //si nos salimos del mapa morimos al instante
    public void fuerisima()
    {
        vida = 0;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
    }
}
