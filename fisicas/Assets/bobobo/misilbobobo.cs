﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class misilbobobo : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    void Start()
    {
        
    }

    //velocidad constante del proyectil
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(7, this.GetComponent<Rigidbody2D>().velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {   
        //al colisionar con donpatch, la pared o el misil del enemigo, nuestro misil desaparece
        if (collision.gameObject.name == "donPatch")
        {
            Destroy(this.gameObject);

        }
        if (collision.gameObject.name == "pared")
        {
            Destroy(this.gameObject);

        }
        if (collision.gameObject.name == "misild(Clone)")
        {
            Destroy(this.gameObject);

        }
    }
}
