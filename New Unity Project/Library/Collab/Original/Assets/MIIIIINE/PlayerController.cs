﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public PlayerData player; //Datos del personaje
    public int vida; //Vida de nuestro personaje
    public GameObject[] corazones; //Array de corazones que se mostraran en pantalla para representar la vida de nuestro personaje
    public GameObject mana; //Recurso que usara nuestro personaje para lanzar hechizos y atacar
    public bool immortal = false; //boleano de inmortalidad ¯\_(ツ)_/¯

    //Hacemos que nuestro personaje tenga de base los siguientes datos
    void Start()
    {

        player.nBombs = 3;
        player.hp = 100;
        player.mana = 100;
        Invoke("Regen", 1);
        vida = 10;
        
    }

    // Si gastas el mana a la hora de atacar, este se reducira de la barra mostrada por pantalla
    void Update()
    {

        //vida.transform.localScale = new Vector3(player.hp / 100, 1f, 1f);
        mana.transform.localScale = new Vector3(player.mana / 100, 1f, 1f);

    }

    //Regeneracion de mana cada segundo
    void Regen()
    {

        if(player.mana >= 95)
        {

            player.mana = 100;

        }
        else
        {

            player.mana += 5;

        }

        Invoke("Regen", 1);

    }

    // Al ser golpeado por algun ataque reducira la vida de nuestro personaje y cambiara los corazones mostrados por pantalla
    public void daño(float damage)
    {

        if (!immortal)
        {

            player.hp -= damage;

            vida--;
            corazones[vida].SetActive(false);

            if (player.hp <= 0)
            {

                Destroy(this.gameObject);

            }

            immortal = true;
            Invoke("Immortality", 1f);

        }

    }

    // Boleano inmortalidad ¯\_(ツ)_/¯
    public void Immortality()
    {

        immortal = false;

    }

}
