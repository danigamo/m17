﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public PlayerData player;
    public int vida;
    public GameObject[] corazones;
    public GameObject mana;
    public bool immortal = false;

    // Start is called before the first frame update
    void Start()
    {

        player.nBombs = 3;
        player.hp = 100;
        player.mana = 100;
        Invoke("Regen", 1);
        vida = 10;
        
    }

    // Update is called once per frame
    void Update()
    {

        //vida.transform.localScale = new Vector3(player.hp / 100, 1f, 1f);
        mana.transform.localScale = new Vector3(player.mana / 100, 1f, 1f);

    }

    void Regen()
    {

        if(player.mana >= 95)
        {

            player.mana = 100;

        }
        else
        {

            player.mana += 5;

        }

        Invoke("Regen", 1);

    }

    public void daño(float damage)
    {

        if (!immortal)
        {

            player.hp -= damage;

            vida--;
            corazones[vida].SetActive(false);

            if (player.hp <= 0)
            {

                Destroy(this.gameObject);

            }

            immortal = true;
            Invoke("Immortality", 1f);

        }

    }

    public void Immortality()
    {

        immortal = false;

    }

}
