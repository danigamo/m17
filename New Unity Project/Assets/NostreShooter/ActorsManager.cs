﻿using System.Collections.Generic;
using UnityEngine;
namespace mio
{
    public class ActorsManager : MonoBehaviour
    {
        public List<Actor> actors { get; private set; }

        private void Awake()
        {
            actors = new List<Actor>();
        }
    }
}