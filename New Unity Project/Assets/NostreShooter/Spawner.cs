﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject doge;
    public GameObject player;
    public GameObject manaspot1;
    public GameObject manaspot2;
    public float minSpawnTime = 5.0f;
    public float maxSpawnTime = 15.0f;
    

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawn", 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn()
    {

        GameObject newDoge = Instantiate(doge);
        newDoge.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z+5);
        newDoge.GetComponent<Pincha>().objetivo = player;
        newDoge.GetComponent<Pincha>().manaspot1 = manaspot1;
        newDoge.GetComponent<Pincha>().manaspot2 = manaspot2;
        float timeGap = Random.Range(minSpawnTime, maxSpawnTime);
        if (minSpawnTime > 1)
        {
            minSpawnTime -= 0.2f;
        }
        if(maxSpawnTime > 3)
        {
            maxSpawnTime -= 0.5f;
        }
        Invoke("Spawn", timeGap);

    }

}
