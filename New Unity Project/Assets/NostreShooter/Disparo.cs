﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject bengala;
    public RaycastHit hit;
    float damage = 10f;
    float range = 50f;
    float momentum = 100f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        StartCoroutine(Fire());
        Destroy(this);

    }

    IEnumerator Fire()
    {
        print(hit.transform.name);
        if (hit.transform.tag == "Disparable")
        {
            hit.transform.gameObject.GetComponent<Disparable>().dano(damage);
            //Vector3 middle = new Vector3(hit.normal.x, )
            hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
        }
        GameObject newBengala = Instantiate(bengala);
        newBengala.transform.position = hit.point;
        yield return new WaitForSeconds(1f);
        Destroy(newBengala);
    }

}
