﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragQueen : MonoBehaviour
{
    public bool cogidon = false;
    public Vector3 pos;
    float x;
    float y;


    void Start()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        x = 10;
        y = 2;
        
    }

    // Update is called once per frame
    void Update()
    {

        if (cogidon == true)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //transform.Translate(mousePos);
            transform.position = new Vector3(mousePos.x, mousePos.y, 0);

        }

    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.gameObject.name == "Plataforma1")
        {
            //pos = new Vector3(10f, -4.3f, 0f);
            pos.x = 10f;
            pos.y = -4.3f;
            x = 10f;
            y = -4.3f;
        }
        if (collision.gameObject.name == "Plataforma2")
        {
            //pos = new Vector3(5.75f, -4.4f, 0f);
            pos.x = 5.75f;
            pos.y = -4.4f;
        }
        if (collision.gameObject.name == "Plataforma3")
        {
            pos = new Vector3(7.5f, -1.5f, 0f);
        }
        if (collision.gameObject.name == "Plataforma4")
        {
            pos = new Vector3(0.75f, -2.9f, 0f);
        }
        if (collision.gameObject.name == "Plataforma5")
        {
            pos = new Vector3(1f, -0.75f, 0f);
        }
        if (collision.gameObject.name == "Plataforma6")
        {
            pos = new Vector3(-3f, -0.75f, 0f);
        }
        if (collision.gameObject.name == "Plataforma8")
        {
            pos = collision.gameObject.transform.position;
        }

    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.name == "Plataforma1")
        {
            Debug.Log(collision.gameObject.name);
            //pos = new Vector3(10f, -4.3f, 0f);
            pos.x = 10f;
            pos.y = -4.3f;
            x = 10f;
            y = -4.3f;
        }
        if (collision.gameObject.name == "Plataforma2")
        {
            pos = new Vector3(5.75f, -4.4f, 0f);
        }
        if (collision.gameObject.name == "Plataforma3")
        {
            pos = new Vector3(7.5f, -1.5f, 0f);
        }
        if (collision.gameObject.name == "Plataforma4")
        {
            pos = new Vector3(0.75f, -2.9f, 0f);
        }
        if (collision.gameObject.name == "Plataforma5")
        {
            pos = new Vector3(1f, -0.75f, 0f);
        }
        if (collision.gameObject.name == "Plataforma6")
        {
            pos = new Vector3(-3f, -0.75f, 0f);
        }
        if (collision.gameObject.name == "Plataforma8")
        {
            pos = collision.gameObject.transform.position;
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {

        if (other.gameObject.name == "Plataforma1")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }
        if (other.gameObject.name == "Plataforma2")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }
        if (other.gameObject.name == "Plataforma3")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }
        if (other.gameObject.name == "Plataforma4")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }
        if (other.gameObject.name == "Plataforma5")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }
        if (other.gameObject.name == "Plataforma6")
        {
            pos = new Vector3(-8.7f, -7.9f, 0f);
        }

    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0)) 
            {
                cogidon = true;
            }
    }

    private void OnMouseUp()
    {

        cogidon = false;


        //this.transform.position = pos;
        this.transform.position = new Vector3(x, y, 0f);

        
    }
}
