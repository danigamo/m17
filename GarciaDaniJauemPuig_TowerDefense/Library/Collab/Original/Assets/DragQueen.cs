﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragQueen : MonoBehaviour
{
    public bool cogidon = false;

    // Update is called once per frame
    void Update()
    {

        if (cogidon == true)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePos);

        }

    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0)) 
            {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            cogidon = true;
            }
    }
    private void OnMouseDrag()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        transform.Translate(mousePos);
    }

    private void OnMouseUp()
    {

        cogidon = false;


        
    }
}
