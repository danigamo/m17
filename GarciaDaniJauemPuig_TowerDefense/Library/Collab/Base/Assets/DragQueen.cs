﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragQueen : MonoBehaviour
{
    public bool cogidon = false;
    public GameObject plataforma;
    

    void Start()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (cogidon == true)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePos);

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Platform")
        {
            plataforma = collision.gameObject;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {

        if (other.gameObject.tag == "Platform")
        {

            plataforma = null;

        }

    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0)) 
            {
                cogidon = true;
            }
    }

    private void OnMouseUp()
    {

        cogidon = false;
        /*
        Debug.Log(plataforma.name);

        if (plataforma != null)
        {
            this.transform.position = plataforma.transform.position;
        }
        else
        {

            this.transform.position = new Vector3(-8.7f, -7.9f, 0f);

        }

        */
    }
}
