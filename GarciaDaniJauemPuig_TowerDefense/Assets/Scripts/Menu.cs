﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    //Segun el boton que pulses activara una funcion u otra que cargara la escena de un mapa
    public void bosque()
    {
        SceneManager.LoadScene("Bosque");//Te llevara a la escena de Bosque
    }
    public void cave()
    {
        SceneManager.LoadScene("Cueva");//Te llevara a la escena de Cueva
    }
    public void desert()
    {
        SceneManager.LoadScene("Deset");//Te llevara a la escena de Desierto
    }
    public void nieve()
    {
        SceneManager.LoadScene("Nieve");//Te llevara a la escena de Nieve
    }
}
